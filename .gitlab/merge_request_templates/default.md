Quality Project Checklist v1.0
- [ ] First checklist item
- [ ] Assignment 7
- [ ] Improve Code
- [ ] Fix CI pipeline
- [ ] Expand the checklist
- [x] Merge a branch
- [x] Merge a branch but with a comment
