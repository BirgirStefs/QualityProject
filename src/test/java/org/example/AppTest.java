package org.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testCombine()
    {
        App a = new App(1);
        App b = new App(1);
        App combined = new App(2);
        App c = a.combine(b);
        assertEquals(combined.getNumber(), c.getNumber());
    }

    @Test
    public void testSubtract()
    {
        App a = new App(2);
        App b = new App(1);
        App subtracted = new App(1);
        App c = a.subtract(b);
        assertEquals(subtracted.getNumber(), c.getNumber());
    }
}
