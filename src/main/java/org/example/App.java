package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    private int number;

    public App(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public App combine(App a) {
        return new App(number + a.getNumber());
    }

    public App subtract(App a) {
        return new App(number - a.getNumber());
    }

    public void dummyMethod(App a) {
        System.out.println("A");
        System.out.println("A");
        System.out.println("A");
        System.out.println("A");
        System.out.println("A");
        System.out.println("A");
        System.out.println("A");

        a.getNumber();
        a.getNumber();
        a.getNumber();
        a.getNumber();
        a.getNumber();
        a.getNumber();
        a.getNumber();

        a.getNumber();
        a.getNumber();
        return;
    }
}
