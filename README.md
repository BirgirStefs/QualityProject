The Quality Project
This project is a work in progress. The project will get bigger as time moves on.
The purpose of this project is to lead me through quality and software quality. 
We will be adding a lot of things regarding quality and how to manage and control with GitLab.
As of now these things are in the project:
- Multiple wiki pages including a Home Page, Software quality management plan page and a license page.
- Milestones that will be for assignments.
- Issues that are connected to certain milestones.
- Source code for our project imported from GitHub
- Labels to specify better what each issue is all about
- More to come...

How to Run:
I am using Maven in Intellij which makes running it really simple. First thing to do is downloading Intellij and Maven3.
Next thing to do is adding this project as a project in Intellij
To build/package the project follow File | Settings/Preferences | Build, Execution, Deployment |Build Tools | Maven and
select Delegate IDE build/run actions to maven.
To run the project you simply build the project first and the click run, assuming you have done it right no errors should appear
In case of errors check the run console for more information on why it is not working.

How to Test:
Since I am using Intellij I will explain how to run in in there. 
- First off build the project
- Go to the Run tab
- Navigate to Run AppTest with coverage
- Press the button
- A pop up probably comes up and asks if you want to add to suite, allow it
- Test results should appear at the bottom and the coverage to the right

CI \
[![pipeline status](https://gitlab.com/BirgirStefs/QualityProject/badges/master/pipeline.svg)](https://gitlab.com/BirgirStefs/QualityProject/-/commits/master) [![coverage report](https://gitlab.com/BirgirStefs/QualityProject/badges/master/coverage.svg)](https://gitlab.com/BirgirStefs/QualityProject/-/commits/master) \

Errors I get in Terminal regarding CI 
- cat: target/site/jacoco/index.html: No such file or directory
- WARNING: target/surefire-reports/TEST-*.xml: no matching files 
- WARNING: target/failsafe-reports/TEST-*.xml: no matching files 

SonarCloud \
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=BirgirStefs_QualityProject&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=BirgirStefs_QualityProject) [![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=BirgirStefs_QualityProject)](https://sonarcloud.io/summary/new_code?id=BirgirStefs_QualityProject) [![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=BirgirStefs_QualityProject)
